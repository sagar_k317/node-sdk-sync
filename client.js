'use strict';
const Tasks = require('./constants/tasks.js');
const Url = require('./constants/url.js')
const Utility = require('./utility/utility.js')
// const request = require("sync-request");
const BadRequestError = require('./errors.js')
class Client extends Tasks{

    constructor(autho ,options=null){
        super();
        this.auth = autho;
        this.DEFAULTS = {};
        this.DEFAULTS['base_url_v2'] = Url.BASE_URL_V2;
        this.DEFAULTS['base_url_v3'] = Url.BASE_URL_V3;
        this.DEFAULTS['headers'] = {};
        this.DEFAULTS.headers['Content-Type'] = 'application/json';
        this.DEFAULTS.headers['apikey'] = this.auth;
        this.base_url = this.set_base_url(options);

        if (options != null){
            this.headers = this.validate_headers(options);
        }
        else{
            this.headers = this.DEFAULTS.headers;
        }
    }

    set_base_url(options) {

        /*
        Set the api_endpoint based on client's initiation
        :param options: dict obj
        :return: api_endpoint url
        */
        var api_endpoint = this.DEFAULTS['base_url_v2'];
        if (options){
            if (options.indexOf('url') >= 0){
                if (typeof(options['url'] != 'string')){
                    throw new BadRequestError("URL is not a string");
                }
                else{
                    api_endpoint = options['url']
                }
            }
        }
        return api_endpoint;
    }

    validate_headers(options){
    /*
        Validate headers and update headers if provided during client initiation
        :param options: kwargs
        :return: headers
    */
        if (options.indexOf('headers') > -1){
            if (isDict(options.headers)){
                if (options.headers.indexOf('apikey') < 0){
                    throw new BadRequestError("No API key provided.");
                }
                for(key in options){
                    this.DEFAULTS.headers[key] = options[key];
                }
            }
            else{
                throw new BadRequestError("Invalid headers provided. Correct format : {'content-Type : 'application/json'}");
            }
        }
        return self.DEFAULTS['headers'];
    }

    post_request(task_type,task_id,data,group_id){
        /*
        post_request to EVE-API
        :param task_type: task_type
        :param task_id: task_id
        :param data: part of the request_body
        :param group_id: group_id
        :return: API_request's request_id and status
        */
       var request = require("sync-request");

       var req_obj = new Utility(task_type,task_id,data,group_id);
       var req_body = req_obj.validate_request_arguments("v2"); //Specify api version here
       var req_options = { 
            headers: this.headers,
            json: req_body };

        console.log(req_body);
        // console.log(this.data);
        var res = request('POST',this.base_url,req_options);
        var user = JSON.parse(res.getBody('utf8'));
        return user;
    }

    get_response(request_id=null,group_id=null,task_id=null){
        /*
        get request to retrieve response from EVE-API with request_id
        :param request_id: request_id
        :param group_id: group_id
        :param task_id: task_id
        :return: API_call's response
        */
       var params={};
       if (request_id === null && group_id === null && task_id === null){
           throw new BadRequestError("Invalid request. Provide atleast one of request_id, task_id or group_id");
       }
       if (request_id != null && typeof(request_id)!= 'string'){
           throw new BadRequestError("Invalid request_id format. Expected format is string.");
       }else{
           params['request_id'] = request_id;
       }
       if (group_id != null && typeof(group_id)!= 'string'){
            throw new BadRequestError("Invalid group_id format. Expected format is string.");
        }else{
            params['group_id'] = group_id;
        }
        if (task_id != null && typeof(task_id)!= 'string'){
            throw new BadRequestError("Invalid task_id format. Expected format is string.");
        }else{
            params['task_id'] = task_id;
        }

        var request = require("sync-request");

        var req_options = {
        qs: params,
        headers: this.headers };

        var res = request('GET',this.base_url,req_options);
        var user = JSON.parse(res.getBody('utf8'));
        return user;
    }
}

let c = new Client("77484e44-db92-4a64-9584-0cc1798cd44e");

var jj= c.post_request("pan_ocr","124",{ 'doc_url': 'https://s3-ap-southeast-1.amazonaws.com/addressify-demo/eve+api+images/Indonesia+NID/ktp.jpg' },"2");
console.log(jj)
// var j = c.get_response("85523312-6d64-45a4-af59-721d0614efc3");
// console.log(j);
// console.log(c.post_request("pan_ocr","124",{ 'doc_url': 'https://s3-ap-southeast-1.amazonaws.com/addressify-demo/eve+api+images/Indonesia+NID/ktp.jpg' },"2"))
// console.log(c.DEFAULTS);
